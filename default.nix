let
  sources = import ./nix/sources.nix {};

  haskellNix = import sources.haskellNix {};

  nixpkgs = import
    haskellNix.sources.nixpkgs-2111
    haskellNix.nixpkgsArgs;

  gitignore-src = import sources.gitignore-src {};
in

with gitignore-src;
with nixpkgs;

rec {
  quibble = haskell-nix.project {
    src = haskell-nix.haskellLib.cleanGit {
      name = "quibble";
      src = gitignoreSource ./.;
    };

    compiler-nix-name = "ghc8107";
  };

  quibble-shell = quibble.shellFor {
    # withHoogle = true;

    tools = {
      cabal = "3.6.2.0";
      # hlint = "latest";
      # stylish-haskell = "latest";
    };

    buildInputs = [
      git
    ];

    exactDeps = true;

    ## Uncomment this if you're running into errors like the following on non-NixOS:
    ##   > commitBuffer: invalid argument (invalid character)
    ## (which basically means that your locale isn't set properly)
    # LOCALE_ARCHIVE = "/usr/lib/locale/locale-archive";
  };
}
