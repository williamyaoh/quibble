## quibble

A backend-agnostic way to write SQL queries.

```haskell
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedLists  #-}
{-# LANGUAGE TypeApplications #-}

query @Foo
  & where_ (#col1 .== inline 4 .&& isNotNull #col2)
  & sortBy [asc #col3]
  & limit 15
```

Truthfully, you probably don't want to use this right now. It's both in a
pretty rough state, and also was purpose built for my own needs. Stay away
for now if you know what's good for you.
